// from: https://www.devx.com/tips/java/finding-the-os-architecture-in-java-using-system.getproperty.html

public class OsArchitecture{

	public static void main(String []args){
		OsArchitecture osArchitecture = new OsArchitecture();
		osArchitecture.proceed();
	}

	private void proceed()
	{
		String osArch = System.getProperty("os.arch");
	    System.out.println(osArch);
			// osArch.indexOf("32") != -1 
			// ?"32 bit architecture":osArch.indexOf("64") != -1 
			// ?"64 bit architecture":"Can't determine");
	}
}
